#lang info

(define collection "GUI-helpers")
(define version "0.1")
(define test-omit-paths '("tests/GUI_helpers_t.rkt"))

(define deps '("base" "gui-lib" "handy" "racket/gui"))

