#lang scribble/manual

@(require
racket/sandbox
scribble/example)


@title{GUI-helpers}
@author{James T. Platt}

@defmodule[GUI-helpers]

@section{Introduction}

@racketmodname[GUI-helpers] contains items to make GUI creation easier and more complete for less effort. The idea is to provide as much as possible by default and require minimal information to modify the defaults or to create additional elements. This includes a spreadsheet-like table tool.  Later, there will be other advanced features.  It is very much a work in progress. 

For example, if you have a frame:

@racketblock[
(define my-frame (new frame% [label "my-frame"] [width 800] [height 600] ))]

Then you can use the make-menus with no optional parameters:
@racketblock[
(make-menus my-frame)]

This will give you default menu items with standard keyboard shortcuts that are universal to all GUI applications. These include a Quit menu item with a keyboard shortcut (command-q on Mac or control-q on Linux and Windows), and Close Window with its own appropriate keyboard shortcut.  

Now, this time, add some optional paramenters.
@racketblock[
(make-menus my-frame
            #:file-open file-open-actions
            #:file-save file-save-actions
            #:file-save-as file-save-as-actions
            #:edit #t ;Edit menu based on the default editor.
            #:font #t ;Font menu based on the default editor. 
)]

Whichever optional parameters you set, a corresponding menu item will be created and will either activate the procdure which you supply or a default procedure as appropriate.

@subsection{Functions}

@defproc[(fill-tab-panel-wth-message [working-tab-panel (is-a?/c tab-panel%)] [message-text string?]) any]{
Puts a message into a tab panel.  This is typically used to fill a tab with default content before anything is clicked in the GUI but it can be used as the callback for hitting a tab. Stretchable width, stretchable hieght and auto resize all default to true.}

@defproc[(make-checklist [parent-container (is-a?/c area-container<%>)]
                         [loh (listof (hash/c symbol? any/c))]
                         [#:columns columns exact-positive-integer?]
                         [#:font font (is-a?/c font%)]
                         )(is-a?/c table-panel%)]{
A checklist is a set of checkboxes arranged in a table-panel.  This table-panel is returned by the make-checklist function.  The table is constructed from a list of hashes where the keys are the labels of the check-boxes and the values are the values of the checkboxes.  You can get the, possibly modified, state of the checklist back as a new list of hashes by calling the function get-checklist-data. If you specify a number of columns, then the number of rows will be computed based on the length of the list of hashes provided.  The font defaults to normal-control-font which is the default for most GUI items in Racket.
}

@defproc[(get-checklist-data [target-checklist (is-a?/c table-panel%)]
                             )(listof (hash/c symbol? boolean?))]{
Return a list of hashes with the status of a checklist.
}

@defproc[(table-from-loh-to-list-box [parent-container (is-a?/c area-container<%>)]
                                     [loh (listof (hash/c symbol? any/c))]
                                     [#:column-order column-order (listof symbol?)]
                                     [#:column-action column-action (procedure?)]
                                     [#:row-single-click-action row-single-click-action (procedure?)]
                                     [#:row-double-click-action row-double-click-action (procedure?)]
                                     ) any]{
This is a data oriented interface for creating GUI tables based on the built-in list-box control of racket/gui. The table is created with content from a list of hashes.  The hashes need to have the same keys and those keys will be used as the column names.  The values of the hashes become the contents of the table.  This is a relatively simple table tool and does not depend on a third-party library.  Most of the time table-from-loh-to-qresults, which requires the qresults-list package, will be the better choice.

Defaults:

Column names are sorted with symbol<? but you can specify any order by supplying a list of symbols where the symbols are the hash keys in loh.

The default action for clicking on a column is to sort by that column but you can supply any procedure to use as the callback.

Actions for single clicking and double clicking on rows, likewise, can be any procedure.  They default to just report the action in a message box.

Limitations:

At this point, you can not edit cells of the table directly.  You have to do something like set up a row double click action which presents another GUI element to edit that row.

}


@;{
table-from-loh-to-qresults isn't ready yet.
@defproc[(table-from-loh-to-qresults [parent-container (is-a?/c area-container<%>)]
                                     [loh (listof (hash/c symbol? any/c))]
                                     [#:column-order column-order (listof symbol?)]
                                     [#:row-double-click-action row-double-click-action (procedure?)]
                                     ) any]{
This is a data oriented interface for creating GUI tables using the qresults-list package.  This is ultimately based on the Racket GUI list-box control but has many enhanced features.  The table is created with content from a list of hashes.  The hashes need to have the same keys and those keys will be used as the column names.  The values of the hashes become the contents of the table.  

The column click action is always to sort by that column.  The sort direction is indicated by an arrow shown in the column header.  This also provides visual feedback as to which column, if any, is used in the current sorting.  Before sorting, data is in whatever order it was originally supplied.  

The row single left click action is to select that row.

The row double left click action, by default, is just to report the action in a message box.  Typically, it will be used to edit a row.

A right click will bring up a row edit menu.  Actions from this menu operate on the selected rows and not necessarily on the row where you actually clicked.  Menu items are enabled and disabled (greyed out) according to whether there is an appropriate selection.


}
}



@defproc[(make-menus  [parent-container (is-a?/c area-container<%>)]
                      [#:file-open file-open-action (or/c #f thunk?)]
                      [#:file-save file-save-action (or/c #f procedure?)]
                      [#:file-save-as file-save-as-action (or/c #f procedure?)]
                      ;  file-close-window is default and does not need to be specified.
                      [#:edit edit boolean?]
                      [#:font font boolean?]
                      ) any]{

By default, in macOS, you get a menu with the application's name.  This menu includes "About <this application>", hide, and quit functions.  There is also a preferences item.  Other menu items are created if you provide an action for them to perform.  


}
